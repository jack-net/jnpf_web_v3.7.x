/*
 * @Description:
 * @Version: 1.0
 * @Author: AaroLi
 * @Date: 2024-08-21 17:27:00
 * @LastEditors: AaroLi
 * @LastEditTime: 2024-09-25 16:25:48
 */
import { defHttp } from '/@/utils/http/axios';
const API = 'api';
// const API = 'ddmApi';
// 获取列表
export function getList(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_statistics/getList`, data });
}
// 新建
export function create(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_statistics`, data });
}
// 修改
export function update(data) {
  return defHttp.put({ url: `/${API}/tendering/Bidding_project_statistics/` + data.id, data });
}
// 详情(无转换数据)
export function getInfo(id) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_statistics/` + id });
}
// 获取(转换数据)
export function getDetailInfo(id) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_statistics/detail/` + id });
}
// 删除
export function del(id) {
  return defHttp.delete({ url: `/${API}/tendering/Bidding_project_statistics/` + id });
}
// 批量删除数据
export function batchDelete(data) {
  return defHttp.delete({ url: `/${API}/tendering/Bidding_project_statistics/batchRemove`, data });
}
// 导出
export function exportData(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_statistics/Actions/Export`, data });
}
