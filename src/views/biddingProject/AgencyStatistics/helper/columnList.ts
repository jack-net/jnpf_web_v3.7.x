/*
 * @Description:
 * @Version: 1.0
 * @Author: AaroLi
 * @Date: 2024-06-18 16:01:10
 * @LastEditors: AaroLi
 * @LastEditTime: 2024-06-19 11:20:46
 */
const columnList = [
  {
    fullName: '上报单位',
    label: '上报单位',
    prop: '上报单位',
    id: '上报单位',
  },
  {
    fullName: '机构数量',
    label: '机构数量',
    prop: '机构数量',
    id: '机构数量',
  },
  {
    fullName: '代理机构一',
    label: '代理机构一',
    prop: '代理机构一',
    id: '代理机构一',
  },
  {
    fullName: '代理机构二',
    label: '代理机构二',
    prop: '代理机构二',
    id: '代理机构二',
  },
  {
    fullName: '随机抽选',
    label: '随机抽选',
    prop: '随机抽选',
    id: '随机抽选',
  },
];
export default columnList;
