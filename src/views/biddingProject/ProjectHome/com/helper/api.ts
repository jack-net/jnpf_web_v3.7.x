/*
 * @Description:
 * @Version: 1.0
 * @Author: AaroLi
 * @Date: 2024-06-21 10:58:30
 * @LastEditors: AaroLi
 * @LastEditTime: 2024-09-25 16:20:34
 */
import { defHttp } from '/@/utils/http/axios';
const API = 'api';
// const API = 'ddmApi';
// 审批/备案项目数
export function getNumbers(id) {
  return defHttp.get({ url: `/${API}/tendering/indicator/projectsNumber?type=${id}&menuId=572406059134626629` });
}
// 项目数 金额
export function getMoneyNumbers(id, type) {
  return defHttp.get({ url: `/${API}/tendering/indicator/moneyAndProjectsNumber?queryDateType=${id}&classificationType=${type}&menuId=572406059134626629` });
}
