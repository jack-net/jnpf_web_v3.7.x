/*
 * @Description:
 * @Version: 1.0
 * @Author: AaroLi
 * @Date: 2024-06-17 16:31:27
 * @LastEditors: AaroLi
 * @LastEditTime: 2024-09-25 16:28:06
 */
import { defHttp } from '/@/utils/http/axios';
const API = 'api';
// const API = 'ddmApi';
// 获取列表
export function getList(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe/getList`, data });
}
// 新建
export function create(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe`, data });
}
// 修改
export function update(data) {
  return defHttp.put({ url: `/${API}/tendering/Bidding_project_subscribe/` + data.id, data });
}
// 详情(无转换数据)
export function getInfo(id) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_subscribe/` + id });
}
// 获取(转换数据)
export function getDetailInfo(id) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_subscribe/detail/` + id });
}
// 删除
export function del(id) {
  return defHttp.delete({ url: `/${API}/tendering/Bidding_project_subscribe/` + id });
}
// 批量删除数据
export function batchDelete(data) {
  return defHttp.delete({ url: `/${API}/tendering/Bidding_project_subscribe/batchRemove`, data });
}
// 导出
export function exportData(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe/Actions/Export`, data });
}
//审核
export function auditData(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe/auditing`, data });
}
//归档
export function archivingData(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe/archivist`, data });
}
// 获取列表表单配置JSON
export function getConfigData(id) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_subscribe` + id });
}
// 获取层级
export function getLevel() {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_subscribe/obtainHierarchy` });
}
//终止
export function terminationData(data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe/termination`, data });
}
//下载全部文件
export function getAllFileInfo(data) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_subscribe/getAllFileInfo?id=` + data });
}
//下载单个文件
export function getFileInfo(id, type) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_subscribe/download/${id}/${type}` });
}
//下载单个文件 body传参
export function getFileInBody(id, type, data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe/download/${id}/${type}`, data });
}
// 获取哪一级的人
export function getPeople(id) {
  return defHttp.get({ url: `/${API}/tendering/Bidding_project_subscribe/obtainHierarchyOfCreate?id=${id}` });
}
