export const baseProps = {
  value: { type: [String, Array] as PropType<String | string[]> },
  multiple: { type: Boolean, default: false },
  placeholder: { type: String, default: '请选择' },
  disabled: { type: Boolean, default: false },
  allowClear: { type: Boolean, default: true },
  size: String,
  selectType: { type: String, default: 'all' },
  buttonType: { type: String as PropType<'' | 'select' | 'button' | undefined>, default: 'select' },
  hasSys: { type: Boolean, default: false },
  ableIds: { type: Array as PropType<string[]>, default: () => [] },
};

export const organizeSelectProps = {
  ...baseProps,
  auth: { type: Boolean, default: false },
  isOnlyOrg: { type: Boolean, default: false },
  currOrgId: { default: '0' },
  parentId: { default: '' },
  modalTitle: { type: String, default: '选择组织' },
};
export const depSelectProps = {
  ...baseProps,
  modalTitle: { type: String, default: '选择部门' },
};
export const posSelectProps = {
  ...baseProps,
  modalTitle: { type: String, default: '选择岗位' },
};
export const roleSelectProps = {
  ...baseProps,
  modalTitle: { type: String, default: '选择角色' },
};
export const groupSelectProps = {
  ...baseProps,
  modalTitle: { type: String, default: '选择分组' },
};
export const userSelectProps = {
  ...baseProps,
  ableRelationIds: { type: Array as PropType<string[]>, default: () => [] },
  modalTitle: { type: String, default: '选择用户' },
};
export const usersSelectProps = {
  ...baseProps,
  modalTitle: { type: String, default: '选择用户' },
};
export interface imgItem {
  name: string;
  fileId: string;
  url: string;
  thumbUrl: string;
}
export interface fileItem extends imgItem {
  fileSize: number | string;
  fileExtension?: string;
  fileVersionId?: string;
}

export const units = {
  KB: 1024,
  MB: 1024 * 1024,
  GB: 1024 * 1024 * 1024,
};
const uploadBaseProps = {
  disabled: { type: Boolean, default: false },
  detailed: { type: Boolean, default: false },
  sizeUnit: { type: String, default: 'MB' },
  fileSize: { type: Number, default: 10 },
  limit: { type: Number, default: 0 },
  tipText: { type: String, default: '' },
  showTip: { type: Boolean, default: false },
  simple: { type: Boolean, default: false },
  showUploadList: { type: Boolean, default: true },
  pathType: { type: String, default: 'defaultPath' },
  sortRule: { type: Object, default: () => [] },
  timeFormat: { type: String, default: 'YYYY' },
  folder: { type: String, default: '' },
};

export const uploadImgProps = {
  ...uploadBaseProps,
  value: { type: Array as PropType<imgItem[]> },
  type: { type: String, default: 'annexpic' },
  accept: { type: String, default: 'image/*' },
  buttonText: { type: String, default: '' },
};
export const uploadFileProps = {
  ...uploadBaseProps,
  value: { type: Array as PropType<fileItem[]>, default: [] },
  type: { type: String, default: 'annex' },
  accept: { type: String, default: '*' },
  buttonText: { type: String, default: '点击上传' },
  showIcon: { type: Boolean, default: true },
  showView: { type: Boolean, default: true },
  showDownload: { type: Boolean, default: true },
};
export const uploadImgSingleProps = {
  value: { type: String, default: '' },
  tipText: { type: String, default: '' },
  subTipText: { type: String, default: '' },
  type: { type: String, default: 'annexpic' },
  accept: { type: String, default: 'image/*' },
  disabled: { type: Boolean, default: false },
  sizeUnit: { type: String, default: 'MB' },
  fileSize: { type: Number, default: 10 },
};
