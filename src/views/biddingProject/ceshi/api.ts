/*
 * @Description:
 * @Version: 1.0
 * @Author: AaroLi
 * @Date: 2024-06-21 10:58:30
 * @LastEditors: AaroLi
 * @LastEditTime: 2024-08-14 18:43:39
 */
import { defHttp } from '/@/utils/http/axios';

const API = 'api';
// const API = 'ddmApi';
// 审批/备案项目数
export function getNumbers(id, year) {
  return defHttp.get({ url: `/${API}/tendering/indicator/projectsNumber?type=${id}&menuId=572406059134626629&corresponding=${year}` });
}
// 项目数 金额
export function getMoneyNumbers(id, type, t1, t2) {
  return defHttp.get({
    url: `/${API}/tendering/indicator/moneyAndProjectsNumber?queryDateType=${id}&classificationType=${type}&startTime=${t1}&endTime=${t2}&menuId=572406059134626629`,
  });
}
//我的待办
export function getTodo() {
  return defHttp.get({ url: `/${API}/tendering/indicator/myToDoList` });
}
