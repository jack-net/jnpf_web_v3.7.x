/*
 * @Description:
 * @Version: 1.0
 * @Author: AaroLi
 * @Date: 2024-06-17 16:31:27
 * @LastEditors: AaroLi
 * @LastEditTime: 2024-09-25 16:29:49
 */
import { defHttp } from '/@/utils/http/axios';
const API = 'api';
// const API = 'ddmApi';
// 分页列表
export function getList(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/queryList`, data });
}
// 分页列表详情
export function queryInfo(data) {
  return defHttp.get({ url: `/${API}/tendering/lottery-project/queryInfo`, data });
}
//组织机构字典
export function dictionaryData() {
  return defHttp.get({ url: `/${API}/system/DictionaryData/dc6b2542d94b5434fc61ec1d59592901` });
}
// 新增抽签项目
export function lotAdd(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/add`, data });
}
// 编辑抽签项目
export function lotEdit(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/edit`, data });
}
// 编辑抽签项目
export function lotDelete(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/delete`, data });
}
//专家组别列表
export function zjData() {
  return defHttp.get({ url: `/${API}/system/DictionaryData/539395626211742999` });
}
//评审详情查询
export function queryReviewInfo(data) {
  return defHttp.get({ url: `/${API}/tendering/lottery-project/queryReviewInfo`, data });
}
// 添加评审
export function addReview(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/addReview`, data });
}
// 修改专家组别评审
export function editReview(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/editReview`, data });
}
// 删除专家组别评审
export function deleteReview(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/deleteReview`, data });
}
//根据类型获取某评审下专家列表
export function getExpertsByExpertGroup(data) {
  return defHttp.get({ url: `/${API}/tendering/lottery-project/getExpertsByExpertGroup`, data });
}
//抽取结果保存
export function saveExtractionResults(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/saveExtractionResults`, data });
}
//抽取结果参加确认
export function confirmation(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/participateInConfirmation`, data });
}
//下载单个文件 body传参
export function getFileInBody(id, type, data) {
  return defHttp.post({ url: `/${API}/tendering/Bidding_project_subscribe/download/${id}/${type}`, data });
}
//结果打印
export function resultPrinting(data) {
  return defHttp.get({ url: `/${API}/tendering/lottery-project/resultPrinting`, data });
}
//上传抽取结果
export function uploadExtractionResults(data) {
  return defHttp.post({ url: `/${API}/tendering/lottery-project/uploadExtractionResults`, data });
}
