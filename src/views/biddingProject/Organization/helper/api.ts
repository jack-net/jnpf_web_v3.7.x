/*
 * @Description:
 * @Version: 1.0
 * @Author: AaroLi
 * @Date: 2024-06-17 16:31:27
 * @LastEditors: AaroLi
 * @LastEditTime: 2024-09-25 16:31:54
 */
import { defHttp } from '/@/utils/http/axios';
const API = 'api';
// const API = 'ddmApi';
// 分页列表
export function getList(data) {
  return defHttp.post({ url: `/${API}/tendering/expert/list`, data });
}
//组织列表
export function getOrganizationList(data) {
  return defHttp.get({ url: `/${API}/tendering/expert/getOrganizationList`, data });
}
//专家组别列表
export function dictionaryData() {
  return defHttp.get({ url: `/${API}/system/DictionaryData/539395626211742999` });
}
//新增专家字典
export function addDictionary(data) {
  return defHttp.post({ url: `/${API}/system/DictionaryData`, data });
}
//编辑专家字典
export function editDictionary(data) {
  return defHttp.put({ url: `/${API}/system/DictionaryData/` + data.id, data });
}
//删除专家字典
export function deleteDictionary(data) {
  return defHttp.delete({ url: `/${API}/system/DictionaryData/` + data.id, data });
}
//新增列表专家
export function addZjList(data) {
  return defHttp.post({ url: `/${API}/tendering/expert/add`, data });
}
//修改列表专家
export function editList(data) {
  return defHttp.post({ url: `/${API}/tendering/expert/edit`, data });
}
//删除列表专家
export function deleteList(data) {
  return defHttp.post({ url: `/${API}/tendering/expert/delete`, data });
}
//详情
export function queryInfo(data) {
  return defHttp.get({ url: `/${API}/tendering/expert/queryInfo`, data });
}
