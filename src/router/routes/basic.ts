import type { AppRouteRecordRaw } from '/@/router/types';
import { t } from '/@/hooks/web/useI18n';
import { REDIRECT_NAME, LAYOUT, EXCEPTION_COMPONENT, PAGE_NOT_FOUND_NAME } from '/@/router/constant';

// 404 on a page
export const PAGE_NOT_FOUND_ROUTE: AppRouteRecordRaw = {
  path: '/:path(.*)*',
  name: PAGE_NOT_FOUND_NAME,
  component: LAYOUT,
  meta: {
    title: 'ErrorPage',
    hideBreadcrumb: true,
    hideMenu: true,
  },
  children: [
    {
      path: '/:path(.*)*',
      name: PAGE_NOT_FOUND_NAME,
      component: EXCEPTION_COMPONENT,
      meta: {
        title: '',
        hideBreadcrumb: true,
        hideMenu: true,
      },
    },
  ],
};

export const REDIRECT_ROUTE: AppRouteRecordRaw = {
  path: '/redirect',
  component: LAYOUT,
  name: 'RedirectTo',
  meta: {
    title: REDIRECT_NAME,
    hideBreadcrumb: true,
    hideMenu: true,
  },
  children: [
    {
      path: '/redirect/:path(.*)/:_redirect_type(.*)/:_origin_params(.*)?',
      name: REDIRECT_NAME,
      component: () => import('/@/views/basic/redirect/index.vue'),
      meta: {
        title: REDIRECT_NAME,
        hideBreadcrumb: true,
      },
    },
  ],
};

export const ERROR_LOG_ROUTE: AppRouteRecordRaw = {
  path: '/error-log',
  name: 'ErrorLog',
  component: LAYOUT,
  redirect: '/error-log/list',
  meta: {
    title: 'ErrorLog',
    hideBreadcrumb: true,
    hideChildrenInMenu: true,
  },
  children: [
    {
      path: 'list',
      name: 'ErrorLogList',
      component: () => import('/@/views/basic/error-log/index.vue'),
      meta: {
        title: t('routes.basic.errorLogList'),
        hideBreadcrumb: true,
        currentActiveMenu: '/error-log',
      },
    },
  ],
};
export const COMMON_ROUTE: AppRouteRecordRaw = {
  path: '/common-route',
  name: 'commonRoute',
  component: LAYOUT,
  redirect: '/home',
  meta: {
    title: 'commonRoute',
    hideBreadcrumb: true,
    hideChildrenInMenu: true,
  },
  children: [
    {
      path: '/home',
      component: () => import('/@/views/basic/home/index.vue'),
      name: 'home',
      meta: {
        title: 'routes.basic.home',
        defaultTitle: '首页',
        icon: 'icon-ym icon-ym-nav-home',
        affix: true,
      },
    },
    {
      path: '/messageRecord',
      component: () => import('/@/views/basic/messageRecord/index.vue'),
      name: 'messageRecord',
      meta: {
        title: 'routes.basic.messageRecord',
        defaultTitle: '站内消息',
        icon: 'icon-ym icon-ym-sysNotice',
      },
    },
    {
      path: '/profile',
      component: () => import('/@/views/basic/profile/index.vue'),
      name: 'profile',
      meta: {
        title: 'routes.basic.profile',
        defaultTitle: '个人信息',
        icon: 'icon-ym icon-ym-user',
      },
    },
    {
      path: '/externalLink',
      component: () => import('/@/views/common/externalLink/index.vue'),
      name: 'externalLink',
      meta: {
        title: 'routes.basic.externalLink',
        defaultTitle: '链接',
        icon: 'icon-ym icon-ym-generator-link',
      },
    },
    {
      path: '/workFlowDetail',
      component: () => import('/@/views/workFlow/workFlowDetail/index.vue'),
      name: 'workFlowDetail',
      meta: {
        title: 'routes.basic.workFlowDetail',
        defaultTitle: '流程详情',
        icon: 'icon-ym icon-ym-workFlow',
      },
    },
    {
      path: '/emailDetail',
      component: () => import('/@/views/extend/email/DetailPage.vue'),
      name: 'emailDetail',
      meta: {
        title: 'routes.basic.emailDetail',
        defaultTitle: '查看邮件',
        icon: 'icon-ym icon-ym-emailExample',
      },
    },
    {
      path: '/previewModel',
      component: () => import('/@/views/common/dynamicModel/index.vue'),
      name: 'previewModel',
      meta: {
        title: 'routes.basic.previewModel',
        defaultTitle: '功能预览',
        icon: 'icon-ym icon-ym-btn-preview',
      },
    },
    {
      path: '/FillingBiddingProjects',
      name: 'FillingBiddingProjects',
      component: () => import('/@/views/biddingProject/FillingBiddingProjects/index.vue'),
      meta: {
        title: '招标项目填报',
      },
    },
    {
      path: '/ProjectQuantityStatistics',
      name: 'ProjectQuantityStatistics',
      component: () => import('/@/views/biddingProject/ProjectQuantityStatistics/index.vue'),
      meta: {
        title: '招标数量统计表',
      },
    },
    {
      path: '/AgencyStatistics',
      name: 'AgencyStatistics',
      component: () => import('/@/views/biddingProject/AgencyStatistics/index.vue'),
      meta: {
        title: '代理机构统计表',
      },
    },
    {
      path: '/ApprovalBiddingProjects',
      name: 'ApprovalBiddingProjects',
      component: () => import('/@/views/biddingProject/ApprovalBiddingProjects/index.vue'),
      meta: {
        title: '招标项目审批',
      },
    },
    {
      path: '/BiddingProjectInquiry',
      name: 'BiddingProjectInquiry',
      component: () => import('/@/views/biddingProject/BiddingProjectInquiry/index.vue'),
      meta: {
        title: '招标项目查询',
      },
    },
    {
      path: '/ProjectAmountStatistics',
      name: 'ProjectAmountStatistics',
      component: () => import('/@/views/biddingProject/ProjectAmountStatistics/index.vue'),
      meta: {
        title: '项目金额统计表',
      },
    },
    {
      path: '/ProjectHome',
      name: 'ProjectHome',
      component: () => import('/@/views/biddingProject/ceshi/index.vue'),
      // component: () => import('/@/views/biddingProject/ProjectHome/index.vue'),
      meta: {
        title: '首页',
      },
    },
    {
      path: '/Management',
      name: 'Management',
      component: () => import('/@/views/biddingProject/Management/index.vue'),
      meta: {
        title: '专家管理',
      },
    },
    {
      path: '/Extraction',
      name: 'Extraction',
      component: () => import('/@/views/biddingProject/Extraction/index.vue'),
      meta: {
        title: '专家抽取',
      },
      children: [
        {
          path: 'detail', // 子路由的路径
          name: 'Extractiondetail',
          component: () => import('/@/views/biddingProject/Extraction/detail.vue'),
          meta: {
            title: '专家抽取',
          },
        },
      ],
    },
    {
      path: '/Organization',
      name: 'Organization',
      component: () => import('/@/views/biddingProject/Organization/index.vue'),
      meta: {
        title: '代理服务机构管理',
      },
    },
    {
      path: '/Company',
      name: 'Company',
      component: () => import('/@/views/biddingProject/Company/index.vue'),
      meta: {
        title: '招标公司抽取',
      },
    },
    {
      path: '/homeNew',
      name: 'homeNew',
      component: () => import('/@/views/biddingProject/ceshi/index.vue'),
      meta: {
        title: '首页',
      },
    },
  ],
};
